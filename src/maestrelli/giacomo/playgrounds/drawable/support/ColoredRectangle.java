package maestrelli.giacomo.playgrounds.drawable.support;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ColoredRectangle extends Rectangle {

	private Color color;

	public ColoredRectangle(int i, int j, int k, int squareDim,Color color) {
		super(i,j,k,squareDim);
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}
