package maestrelli.giacomo.playgrounds.drawable;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Tetramino extends Renderizzato<GraphicsContext,PlayField>{

	
	public enum RelativeRotation{L,Z,R,T,undefined; 
		
		public RelativeRotation newPosition(boolean counterClockwise)
		{
			
			switch(this)
			{
				case L:return counterClockwise?  T:Z;
				case R:return counterClockwise?  Z:T;
				case T:return counterClockwise?  R:L;
				case Z:return counterClockwise?  L:R;
				default:return Z;			
			}
		}
	}
	
	public enum TetraType{spiral('a'),wavein('b'),waveout('c'),crossin('d'),crossout('e'),square('f'),longjohn('g');
	
		private final boolean[][] internalRep;
		
		TetraType(char a)
		{
			internalRep = new boolean[4][4];
			internalRep[1][1]=internalRep[1][2]=internalRep[2][1]=true;
			switch(a)
			{
				case 'a':internalRep[1][0]=true;break;
				case 'b':internalRep[3][1]=true;break;
				case 'c':internalRep[1][3]=true;break;
				case 'd':internalRep[2][0]=true;break;
				case 'e':internalRep[0][2]=true;break;
				case 'f':internalRep[2][2]=true;break;
				case 'g':internalRep[1][2]=false;
						 internalRep[0][1]=true;
						 internalRep[3][1]=true;break;
			}
		}
		
		public boolean[][] getRep()
		{
		    boolean[][] result = new boolean[internalRep.length][];
		    for (int r = 0; r < internalRep.length; r++) {
		        result[r] = internalRep[r].clone();
		    }
		    return result;
		};	
		
		public Color getColor() 
		{
			switch(this)
			{	
				case spiral:return Color.ORANGE;
				case wavein:return Color.MAGENTA;
				case waveout:return Color.YELLOW;
				case crossin:return Color.GREEN;
				case crossout:return Color.CYAN;
				case square:return Color.BLUE;
				case longjohn:return Color.RED;
			}
			return Color.BLACK;
		}
	};
	
	/*fix Centro di rotazione*/
	private RelativeRotation rr;
	private int ruotaAttuale;
	private TetraType tType;
	
	public Tetramino(TetraType tt)
	{
		super(tt.getRep());
		this.tType = tt;
		ruotaAttuale=0;
		rr=RelativeRotation.undefined;
	}
	

	public void overwriteStatus(Tetramino toCopy)
	{
		boolean [][] destin = toCopy.tType.getRep();
		boolean [][] source = this.getInternalStatus();
		for(int i,j=0;j<destin.length;j++)
		for(i=0;i<destin[j].length;i++)
			source[j][i] = destin[j][i];
		this.tType = toCopy.gettType();
		resetRuotaAttuale();
	}
	
	public void ruotaMemFree(boolean antiClockWise)
	{
		ruotaAttuale=(((antiClockWise?1:-1)+4)+ruotaAttuale)%4;
		rr= rr.newPosition(antiClockWise);
		/*NO heap Allocation on this  method*/
		boolean[][] toRotate = this.getInternalStatus();
		
		boolean tempA = toRotate[rotationChains[0][antiClockWise? 3:0][1]][rotationChains[0][antiClockWise? 3:0][0]];
		boolean tempB = toRotate[rotationChains[1][antiClockWise? 3:0][1]][rotationChains[1][antiClockWise? 3:0][0]];
		boolean tempC = toRotate[rotationChains[2][antiClockWise? 3:0][1]][rotationChains[2][antiClockWise? 3:0][0]];
		boolean tempD = toRotate[rotationChains[3][antiClockWise? 3:0][1]][rotationChains[3][antiClockWise? 3:0][0]];
		
		for(int h=0;h<rotationChains.length;h++)
		{	
			for(int k=rotationChains[h].length-1;antiClockWise&&k>0;k--)
				toRotate[rotationChains[h][k][1]][rotationChains[h][k][0]]
					=toRotate[rotationChains[h][k-1][1]][rotationChains[h][k-1][0]];
			for(int k=0;!antiClockWise&&k<rotationChains[h].length-1;k++) 
				toRotate[rotationChains[h][k][1]][rotationChains[h][k][0]]
					=toRotate[rotationChains[h][k+1][1]][rotationChains[h][k+1][0]];
		}
		
		toRotate[rotationChains[0][antiClockWise? 0:3][1]][rotationChains[0][antiClockWise? 0:3][0]] = tempA;
		toRotate[rotationChains[1][antiClockWise? 0:3][1]][rotationChains[1][antiClockWise? 0:3][0]] = tempB;
		toRotate[rotationChains[2][antiClockWise? 0:3][1]][rotationChains[2][antiClockWise? 0:3][0]] = tempC;
		toRotate[rotationChains[3][antiClockWise? 0:3][1]][rotationChains[3][antiClockWise? 0:3][0]] = tempD;
	}
	
	public int getRuotaAttuale() {
		return ruotaAttuale;
	}

	public void resetRuotaAttuale() {
		this.ruotaAttuale = 0;
	}

	
	public void setNewStatus(boolean[][] newStatus)
	{
		if(newStatus!=null)
			super.setStatus(newStatus);
	}
	
	public TetraType gettType() {
		return tType;
	}
	
	public void setRr(RelativeRotation rr) {
		this.rr = rr;
	}

	public RelativeRotation getRr() {
		return rr;
	}
	
	private static int[][][] rotationChains = 
		{
			{{0,0},{0,3},{3,3},{3,0}},
			{{1,0},{0,2},{2,3},{3,1}},
			{{2,0},{0,1},{1,3},{3,2}},
			{{1,1},{1,2},{2,2},{2,1}},
		};


}
