package maestrelli.giacomo.playgrounds.drawable;


abstract class Renderizzato<G,A> {

	private boolean [][] status;
	
	protected Renderizzato(byte i,byte j) {
		if(i<=0&&j<0)throw new IllegalArgumentException("NO");
		status = new boolean[j][i];
	}
	
	protected Renderizzato(boolean[][] status) {
		this.status = status;
	}
	
	private String getInternalRep()
	{	
		StringBuffer sb = new StringBuffer(status[0].length*status.length);
		for(char i,j=0;j<status.length;j++)
		{
			for(i=0;i<status[j].length;i++)
				sb.append(status[j][i]?"X":"O" );
			sb.append("\n");
		}
		return sb.toString();
	}
	
	protected void setStatus(boolean[][] newstatus)
	{ 
		this.status = newstatus;
	}
	
	protected boolean getPointPresence(byte x,byte y)
	{
		return this.status[y][x];
	}
	
	protected void setPointPresence(byte x,byte y,boolean status)
	{
		this.status[y][x]=status;
	}
	
	protected boolean[][] getInternalStatus()
	{
		return status; 
	}
	
	
	@Override
	public String toString()
	{
		return getInternalRep();
	}
	
	
	public int getComposedAsRectDimY()
	{
		return status.length;
	}
	
	public int getComposedAsRectDimX()
	{
		return status[0].length;
	}
	

}
