package maestrelli.giacomo.playgrounds.drawable;

import maestrelli.giacomo.playgrounds.drawable.PlayField.REQUESTOPERATION;

public class MovingTetra extends Tetramino {

	private int xActual;
	private int yActual;
	
	public MovingTetra(TetraType tt) {
		super(tt);
		xActual = 0;
		yActual = 0;
	}

	public int getxActual() {
		return xActual;
	}

	public void setxActual(int xActual) {
		this.xActual = xActual;
	}

	public int getyActual() {
		return yActual;
	}

	public void setyActual(int yActual) {
		this.yActual = yActual;
	}

	public void superRotationSystemTranslation(REQUESTOPERATION operation, boolean reverse,int i) {
		RelativeRotation rr = this.getRr();
		 /*superotation Matrix - No Longjohn*/
		if(!TetraType.longjohn.equals(this.gettType()))
			switch(rr)
			{
			case L:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse)
							this.xActual=this.xActual+1;
						else 
							this.xActual=this.xActual-1;
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual-1;
						}
						else 
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual+1;
						}
						break;
					case 3:
						if(!reverse){this.yActual=this.yActual+2;}
						else {this.yActual=this.yActual-2;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual+2;
						}
						else 
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual-2;
						}
				}
				break;
			case R:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse)
							this.xActual=this.xActual-1;
						else 
							this.xActual=this.xActual+1;
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual-1;
						}
						else 
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual+1;
						}
						break;
					case 3:
						if(!reverse){this.yActual=this.yActual+2;}
						else {this.yActual=this.yActual-2;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual+2;
						}
						else 
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual-2;
						}
				}
				break;
			case T:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse){this.xActual=this.xActual-1;}
						else{this.xActual=this.xActual+1;}
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual+1;
						}
						else 
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual-1;
						}
						break;
					case 3:
						if(!reverse){this.yActual=this.yActual-2;}
						else {this.yActual=this.yActual+2;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual-2;
						}
						else 
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual+2;
						}
				}
				break;
			case Z:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse){this.xActual=this.xActual+1;}
						else{ this.xActual=this.xActual-1;}
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual+1;
						}
						else 
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual-1;
						}
						break;
					case 3:
						if(!reverse){this.yActual=this.yActual-2;}
						else {this.yActual=this.yActual+2;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual-2;
						}
						else 
						{
							this.xActual=this.xActual-1;									
							this.yActual=this.yActual+2;
						}
				}
				break;
			default:
				break;
	
			}
		else {//LONGJOHN superrotation MATRIX
			switch(rr)
			{
			case L:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse)
							this.xActual=this.xActual-1;
						else 
							this.xActual=this.xActual+1;
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual+2;
							//this.yActual=this.yActual;
						}
						else 
						{
							this.xActual=this.xActual-2;
							//this.yActual=this.yActual+1;
						}
						break;
					case 3:
						if(!reverse){
							this.xActual=this.xActual-1;
							this.yActual=this.yActual-2;
							}
						else {
							this.xActual=this.xActual+1;
							this.yActual=this.yActual+2;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual+2;
							this.yActual=this.yActual+1;
						}
						else 
						{
							this.xActual=this.xActual-2;
							this.yActual=this.yActual-1;
						}
				}
				break;
			case R:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse)
							this.xActual=this.xActual+1;
						else 
							this.xActual=this.xActual-1;
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual-2;
							//this.yActual=this.yActual-2;
						}
						else 
						{
							this.xActual=this.xActual+2;
							//this.yActual=this.yActual+2;
						}
						break;
					case 3:
						if(!reverse){
							this.xActual=this.xActual+1;
							this.yActual=this.yActual+2;}
						else {
							this.xActual=this.xActual-1;
							this.yActual=this.yActual-2;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual-2;
							this.yActual=this.yActual-1;
						}
						else 
						{
							this.xActual=this.xActual+2;
							this.yActual=this.yActual+1;
						}
				}
				break;
			case T:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse){this.xActual=this.xActual-2;}
						else{this.xActual=this.xActual+2;}
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual+1;
							//this.yActual=this.yActual+1;
						}
						else 
						{
							this.xActual=this.xActual-1;
							//this.yActual=this.yActual-1;
						}
						break;
					case 3:
						if(!reverse){
							this.xActual=this.xActual-2;
							this.yActual=this.yActual+1;}
						else {
							this.xActual=this.xActual+2;
							this.yActual=this.yActual-1;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual+1;
							this.yActual=this.yActual-2;
						}
						else 
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual+2;
						}
				}
				break;
			case Z:
				switch(i)
				{
					case 0:break;
					case 1:
						if(!reverse){this.xActual=this.xActual+2;}
						else{ this.xActual=this.xActual-2;}
						break;
					case 2:
						if(!reverse)
						{
							this.xActual=this.xActual-1;
							//this.yActual=this.yActual+1;
						}
						else 
						{
							this.xActual=this.xActual+1;
							//this.yActual=this.yActual-1;
						}
						break;
					case 3:
						if(!reverse){
							this.xActual=this.xActual+2;
							this.yActual=this.yActual-1;}
						else {
							this.xActual=this.xActual-2;
							this.yActual=this.yActual+1;}
						break;
					case 4: 	
						if(!reverse)
						{
							this.xActual=this.xActual-1;
							this.yActual=this.yActual+2;
						}
						else 
						{
							this.xActual=this.xActual+1;									
							this.yActual=this.yActual-2;
						}
				}
				break;
			default:
				break;
	
			}	
		}
	}
	
}
