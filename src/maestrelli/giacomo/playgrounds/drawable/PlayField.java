package maestrelli.giacomo.playgrounds.drawable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import maestrelli.giacomo.playgrounds.drawable.Tetramino.RelativeRotation;
import maestrelli.giacomo.playgrounds.drawable.Tetramino.TetraType;

/**
 * It Defines the playfield status 
 * 
 * @author Giacomo
 *
 */
public class PlayField extends Renderizzato<GraphicsContext,Canvas>
{
	/*start of STATIC INITIALIZATION CLASS*/
	
	/*next piece */
	private static Tetramino[] pieces =new Tetramino[TetraType.values().length];
	public final static MovingTetra[] pieceForecast = new MovingTetra[maestrelli.giacomo.ep.Configuration.MAXFORECASTPIECES];
	
	/*PlayField Enum operations defines allowed rules to play the game, Operations are classified in */
	public enum REQUESTOPERATION{XTranslationL,XTranslationR,YTranslation,ClockWiseRotation,CounterClockWiseRotation,DropOne,DropTotal,ClimbOne,Pause};
	
	public enum RESPONSEOPERATION{OK,NOK,StopPiece,EndGame,NewPiece};
	
	
	private final static Set<REQUESTOPERATION> groupMoves;
	@SuppressWarnings("unused")
	private final static Set<REQUESTOPERATION> translations;
	@SuppressWarnings("unused")
	private final static Set<REQUESTOPERATION> rotations;
	
	static {
		
		groupMoves = new HashSet<REQUESTOPERATION>(Arrays.asList(new REQUESTOPERATION[]{
				REQUESTOPERATION.CounterClockWiseRotation,
				REQUESTOPERATION.ClockWiseRotation,
				REQUESTOPERATION.XTranslationL,
				REQUESTOPERATION.XTranslationR,
				REQUESTOPERATION.DropOne,
				REQUESTOPERATION.DropTotal,
				REQUESTOPERATION.ClimbOne,
				REQUESTOPERATION.YTranslation})) ; 
		
		translations = new HashSet<REQUESTOPERATION>(Arrays.asList(new REQUESTOPERATION[]{
				REQUESTOPERATION.XTranslationL,
				REQUESTOPERATION.XTranslationR,
				REQUESTOPERATION.DropOne,
				REQUESTOPERATION.DropTotal,
				REQUESTOPERATION.ClimbOne,
				REQUESTOPERATION.YTranslation}));
		
		rotations = new HashSet<REQUESTOPERATION>(Arrays.asList(new REQUESTOPERATION[]{
				REQUESTOPERATION.CounterClockWiseRotation,
				REQUESTOPERATION.ClockWiseRotation,
				}));
		
		Random rm = new Random();
		TetraType[] tta = TetraType.values();
		for(int i=0;i<tta.length;i++)
			pieces[i]= (new Tetramino(tta[i]));
		for(int i=0;i<pieceForecast.length;i++)
			pieceForecast[i] = new MovingTetra(TetraType.values()[rm.nextInt(7)]);
		
	}
	/*end of STATIC INITIALIZATION CLASS*/
	private Color[][] colori=null;
	private MovingTetra mt;
	private Random rnd = null;
	ArrayList<Integer> indexToRemove;
	
	public PlayField()
	{
		this((byte)12,(byte)24);
		colori = new Color[24][12];
		for(byte i = 0;i<(byte)12;i++)
		{	
			this.setPointPresence((byte)i, (byte)0, false);
			colori[0][i]=Color.BROWN;
		}
		for(byte i = 0;i<(byte)12;i++) {
			this.setPointPresence((byte)i, (byte)0, true);
			colori[0][i]=Color.BROWN;
			
			this.setPointPresence((byte)i, (byte)23, true);
			colori[23][i]=Color.BROWN;
		}
			
		for(byte j=0;j<(byte)23;j++)
		{
	         this.setPointPresence((byte)0, j, true);
	         colori[j][0]=Color.BROWN;
	 
			this.setPointPresence((byte)11, j, true);
			colori[j][11]=Color.BROWN;
		}
		for(int j=0;j<colori.length;j++)
			for(int i=0;i<colori[j].length;i++)
			{
				if(colori[j][i]==null||!colori[j][i].equals(Color.BROWN))
					colori[j][i]=Color.BLACK;
			}
		rnd = new Random();
		mt=new MovingTetra(TetraType.longjohn);
		indexToRemove = new ArrayList<>();
		newPieceIncoming();
	}

	private PlayField(byte i, byte j)
	{
		super(i, j);
		
	}
	

	public void newPieceIncoming() 
	{	
		mt.overwriteStatus(mt);
		mt.overwriteStatus(pieceForecast[0]);
		for(int i=1;i<pieceForecast.length;i++)
			pieceForecast[i-1].overwriteStatus(pieceForecast[i]);
		pieceForecast[pieceForecast.length-1].overwriteStatus(pieces[rnd.nextInt(7)]);
		int xActual=0, yActual=0;
		switch(mt.gettType())
		{
			case spiral:case crossin:
				xActual = 3;yActual=-1;mt.setRr(RelativeRotation.Z);break;
			case waveout:
				xActual = 2;yActual=-1;mt.setRr(RelativeRotation.T);break;
			case wavein:xActual =3;yActual=-1;mt.ruotaMemFree(false);mt.setRr(RelativeRotation.T);break;
			case crossout:xActual = 3;yActual=-1;mt.ruotaMemFree(false);mt.setRr(RelativeRotation.Z);break;
			case square:xActual = 3;yActual=-1;mt.setRr(RelativeRotation.Z);;break;
			case longjohn:xActual = 3;yActual=0;mt.ruotaMemFree(false);mt.setRr(RelativeRotation.Z);break;
		}
		mt.setxActual(xActual);
		mt.setyActual(yActual);
		
	}
	
	public MovingTetra getMt()
	{
		return mt;
	}
	
	public void renderizza(GraphicsContext g,Canvas area)
	{
		
	}
	
	public int defineSquareDim(Canvas jf)
	{	
		int firstDim = (int)Math.sqrt( (double)((jf.getHeight()*jf.getWidth())/( 3*this.getComposedAsRectDimX() * this.getComposedAsRectDimY() )));
		while( (firstDim*(this.getComposedAsRectDimX() + 2*(pieceForecast[0].getComposedAsRectDimX()+2))> jf.getWidth() || firstDim*(this.getComposedAsRectDimY()+4)>jf.getHeight())&&firstDim>=1)--firstDim;
		return firstDim;
	}
	

	@SuppressWarnings("incomplete-switch")
	public  RESPONSEOPERATION applyingRules(REQUESTOPERATION operation,AudioClip fxSound) {
		if(REQUESTOPERATION.Pause.equals(operation))return RESPONSEOPERATION.StopPiece;
		boolean saldatura = false;		
		do {
			if (groupMoves.contains(operation)) {
				int nR =0;
				boolean continua = true;
				do {
					continua  = true;
					switch (operation)
					{
						case CounterClockWiseRotation:
						case ClockWiseRotation:
							getMt().ruotaMemFree(operation == PlayField.REQUESTOPERATION.CounterClockWiseRotation);
							if (PlayField.REQUESTOPERATION.CounterClockWiseRotation.equals(operation))
								fixCounterClockWiseCenterOfRotation(false);
							else
								fixClockWiseCenterOfRotation(false);
							getMt().superRotationSystemTranslation(operation,false,nR);
							break;
						case ClimbOne:
						case DropOne:
						case DropTotal:
						case YTranslation:
							getMt().setyActual(
									getMt().getyActual() + (operation == PlayField.REQUESTOPERATION.ClimbOne ? -1 : 1));
							break;
						case XTranslationL:
						case XTranslationR:
							getMt().setxActual(
									getMt().getxActual() + (operation == PlayField.REQUESTOPERATION.XTranslationL ? -1 : +1));
							break;
					}

					for (int j = 0; j < getMt().getComposedAsRectDimY() && continua; j++) {
						for (int i = 0; i < getMt().getComposedAsRectDimX() && continua; i++) {
							int xTransform = getMt().getxActual() + i + 1;
							int yTransform = getMt().getyActual() + j + 1;
							if (getMt().getPointPresence((byte) i, (byte) j) && (xTransform < 0 || yTransform < 0
									|| xTransform > getComposedAsRectDimX() - 2 || yTransform > getComposedAsRectDimY() - 2
									|| this.getPointPresence((byte) xTransform, (byte) yTransform))) {
								switch (operation) 
								{
									case CounterClockWiseRotation:
									case ClockWiseRotation:
										getMt().superRotationSystemTranslation(operation,true,nR);
										if (PlayField.REQUESTOPERATION.CounterClockWiseRotation.equals(operation))
											fixCounterClockWiseCenterOfRotation(true);
										else
											fixClockWiseCenterOfRotation(true);
										getMt().ruotaMemFree(operation != PlayField.REQUESTOPERATION.CounterClockWiseRotation);
										break;
									case ClimbOne:
									case DropOne:
									case DropTotal:
									case YTranslation:
										getMt().setyActual(getMt().getyActual()
												+ (operation == PlayField.REQUESTOPERATION.ClimbOne ? 1 : -1));
										saldatura = true;
										break;
									case XTranslationL:
									case XTranslationR:
										getMt().setxActual(getMt().getxActual()
												+ (operation == PlayField.REQUESTOPERATION.XTranslationL ? 1 : -1));
										break;
								}
								continua = false;
							}
						
						}
					}
				}while(++nR<=4&&rotations.contains(operation)&&!continua);

				/* salda */
				if (saldatura) {
					for (int j = 0; j < getMt().getComposedAsRectDimY(); j++)
						for (int i = 0; i < getMt().getComposedAsRectDimX(); i++) {
							if (getMt().getPointPresence((byte) i, (byte) j) && i + getMt().getxActual() + 1 >= 0
									&& i + getMt().getxActual() + 1 <= getComposedAsRectDimX() - 2
									&& j + getMt().getyActual() + 1 <= getComposedAsRectDimY() - 2
									&& j + getMt().getyActual() + 1 >= 0) {
								this.colori[j + getMt().getyActual() + 1][i + getMt().getxActual() + 1] = getMt()
										.gettType().getColor();
								this.setPointPresence((byte) (i + getMt().getxActual() + 1),
										(byte) (j + getMt().getyActual() + 1), true);
							}
						}

					/* elimina le linee complete */
					int k = getComposedAsRectDimY() - 2;
					for (int j = getComposedAsRectDimY() - 2; j > 0; j--) {
						boolean rigaCompleta = true;
						for (int i = 1; i <= getComposedAsRectDimX() - 2; i++) {
							rigaCompleta = rigaCompleta && getPointPresence((byte) i, (byte) j);
							setPointPresence((byte) i, (byte) k, getPointPresence((byte) i, (byte) j));
							colori[k][i] = colori[j][i];
						}
						if (!rigaCompleta) {
							k--;
						} else {
							if (!fxSound.isPlaying())
								fxSound.play();
						}
					}
					// System.out.println(this);
					/* IF CHECK SPAWN NEWPIECE IS TRUE */
					return RESPONSEOPERATION.NewPiece;
					/* ELSE EndGame */
				}

			}
		} while (REQUESTOPERATION.DropTotal.equals(operation) && !saldatura);
		return RESPONSEOPERATION.OK;
	}

	public Color[][] getColori() {
		return colori;
	}


	private void fixCounterClockWiseCenterOfRotation(boolean rollBack) {
		int j = rollBack?-1:1;
		
		switch(getMt().gettType())
		{
			case crossin:case wavein:
				switch(getMt().getRuotaAttuale())
				{
					case 0:getMt().setyActual(getMt().getyActual()-1*j);break;
					case 1:getMt().setxActual(getMt().getxActual()-1*j);break;
					case 2:getMt().setyActual(getMt().getyActual()+1*j);break;
					case 3:getMt().setxActual(getMt().getxActual()+1*j);break;
				}
				break;
			case crossout:case waveout:
				switch(getMt().getRuotaAttuale())
				{
					case 0:getMt().setyActual(getMt().getyActual()+1*j);break;
					case 1:getMt().setxActual(getMt().getxActual()+1*j);break;
					case 2:getMt().setyActual(getMt().getyActual()-1*j);break;
					case 3:getMt().setxActual(getMt().getxActual()-1*j);break;
				}
				break;
			case spiral:
				switch(getMt().getRuotaAttuale())
				{
					case 0:getMt().setxActual(getMt().getxActual()+1*j);break;
					case 1:getMt().setyActual(getMt().getyActual()-1*j);break;
					case 2:getMt().setxActual(getMt().getxActual()-1*j);break;
					case 3:getMt().setyActual(getMt().getyActual()+1*j);break;
				}
				break;
			
			default:
				break;
		}
	}
	
	private void fixClockWiseCenterOfRotation(boolean rollBack) {
		int j = rollBack?-1:1;
		switch(getMt().gettType())
		{
			case crossin:case crossout:case spiral:
				switch(getMt().getRuotaAttuale())
				{
					case 0:getMt().setyActual(getMt().getyActual()+1*j);break;
					case 1:getMt().setxActual(getMt().getxActual()+1*j);break;
					case 2:getMt().setyActual(getMt().getyActual()-1*j);break;
					case 3:getMt().setxActual(getMt().getxActual()-1*j);break;
				}
				break;
			case wavein:
				switch(getMt().getRuotaAttuale())
				{
					case 0:getMt().setxActual(getMt().getxActual()+1*j);break;
					case 1:getMt().setyActual(getMt().getyActual()-1*j);;break;
					case 2:getMt().setxActual(getMt().getxActual()-1*j);break;
					case 3:getMt().setyActual(getMt().getyActual()+1*j);break;
				}
				break;
			case waveout:
				switch(getMt().getRuotaAttuale())
				{
					case 0:getMt().setxActual(getMt().getxActual()-1*j);break;
					case 1:getMt().setyActual(getMt().getyActual()+1*j);;break;
					case 2:getMt().setxActual(getMt().getxActual()+1*j);break;
					case 3:getMt().setyActual(getMt().getyActual()-1*j);break;
				}
				break;
			default:
				break;
		}
	}

}
