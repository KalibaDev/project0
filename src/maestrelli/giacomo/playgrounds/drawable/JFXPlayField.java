package maestrelli.giacomo.playgrounds.drawable;

import java.awt.Paint;
import java.net.MalformedURLException;
import java.net.URISyntaxException;


import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.FillRule;
import javafx.scene.shape.StrokeLineCap;
import maestrelli.giacomo.ep.Configuration;
import maestrelli.giacomo.playgrounds.drawable.PlayField.REQUESTOPERATION;

public class JFXPlayField extends Region {

	/* View */
	private MyCanvas canvas;
	/* Model */
	private PlayField pf;
	
	private AnimationTimer at;
	/* Audio */
	private MediaPlayer[] mp;
	private AudioClip fxSound;
	private AudioClip fxClick;
	private long checkDiff =System.nanoTime();
	private float elapsedTime =0;
	private boolean paused = false;
	
	
	
	public void setPaused(boolean paused) {
		this.paused = paused;
	}


	public JFXPlayField() throws MalformedURLException, URISyntaxException {
		super();
		canvas = new MyCanvas();
		canvas.setFocusTraversable(false);
		canvas.getGraphicsContext2D().setFillRule(FillRule.EVEN_ODD);
		canvas.getGraphicsContext2D().setGlobalAlpha(Paint.TRANSLUCENT);
		canvas.getGraphicsContext2D().setStroke(Color.BLACK);
		canvas.getGraphicsContext2D().setLineCap(StrokeLineCap.BUTT);
		canvas.getGraphicsContext2D().setLineWidth(1);
		canvas.widthProperty().bind(this.widthProperty());
		canvas.heightProperty().bind(this.heightProperty());
		widthProperty()
				.addListener(evt ->draw(true,0,2,pf.getComposedAsRectDimX()-3,pf.getComposedAsRectDimY()-3));
		heightProperty()
				.addListener(evt ->draw(true,0,2,pf.getComposedAsRectDimX()-3,pf.getComposedAsRectDimY()-3));
		focusedProperty().addListener((obs, wasFocused, isNowFocused)->{
			paused = !isNowFocused;
		});
		
		getChildren().add(canvas);
		pf = new PlayField();
		String[] res = new String[4];
		int k =1;
		for(;k<=res.length;k++) 
			res[k-1] = this.getClass().getResource("/00"+k+"_T.mp3").toURI().toURL().toString();
				
		String resFx  =this.getClass().getResource("/sfx_wpn_laser2.wav").toURI().toURL().toString();
		//String resClick  =this.getClass().getResource("/sfx_click.wav").toURI().toURL().toString();
		fxSound = new AudioClip(resFx);
		//fxClick = new AudioClip(resClick);
				
		mp = new MediaPlayer[res.length];
		for(int i = 0;i<mp.length;i++) {
			mp[i]=new MediaPlayer(new Media(res[i]));
		}

	    // play each audio file in turn.

		for (int i = 0; i < mp.length; i++) {

			final MediaPlayer player = mp[i];
			final MediaPlayer nextPlayer = mp[(i + 1) % mp.length];
			player.setOnEndOfMedia(new Runnable() {
				@Override
				public void run() {
					player.stop();
					nextPlayer.play();
				}

			});

		}
		at = new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				
				if(!paused) {
					elapsedTime += (now - checkDiff) / 1000000000.0f;
					if(elapsedTime>Configuration.SPEED) {
						elapsedTime = 0;
						gameEvaluator(REQUESTOPERATION.DropOne);
					}
					checkDiff=now;
				}
			}
		};
	
		if(Configuration.bgm) mp[2].play();
		this.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				Integer i = Configuration.hmKI.get(event.getCode());
				if(fxClick!=null)fxClick.play();
				switch(i!=null?i:Configuration.FIRSTUNUSED)
				{
					case Configuration.KEYUP:gameEvaluator(REQUESTOPERATION.CounterClockWiseRotation);break;
					case Configuration.KEYDOWN:
							if(Configuration.gravity) {elapsedTime+=0.75*Configuration.SPEED;}
							else gameEvaluator(REQUESTOPERATION.YTranslation);
							break;
					case Configuration.KEYLEFT:gameEvaluator(REQUESTOPERATION.XTranslationL); break;
					case Configuration.KEYRIGHT:gameEvaluator(REQUESTOPERATION.XTranslationR); break;
					case Configuration.W: gameEvaluator(REQUESTOPERATION.ClimbOne);break;
					case Configuration.SPACE:gameEvaluator(REQUESTOPERATION.DropTotal);break;
					case Configuration.PAUSE:paused =!paused;
				}
				
			}
		});
		if(Configuration.gravity)at.start();
	}

	
	private void gameEvaluator(PlayField.REQUESTOPERATION operation)
	{
		if(paused)operation = REQUESTOPERATION.Pause;
		switch (pf.applyingRules(operation,fxSound))
		{
			case NewPiece:
				pf.newPieceIncoming();
			case OK:case StopPiece:
				draw(true,0, 2, pf.getComposedAsRectDimX() - 3, pf.getComposedAsRectDimY() - 3);
				break;
				
			case EndGame:
				break;
			default:
				break;
		}
	}
	
	public MyCanvas getCanvas() {
		return canvas;
	}

	@Override
	public ObservableList<Node> getChildren() {
		return super.getChildren();
	}

	public void draw(boolean initial, int xInit, int yInit, int xFinal, int yFinal) {
		if (initial) {
			canvas.getGraphicsContext2D().setFill(Color.AZURE);
			canvas.getGraphicsContext2D().fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
		}
		int squareUnit = pf.defineSquareDim(canvas);
		double startPlayFieldXPosition = squareUnit
				+ ((canvas.getWidth() - squareUnit * pf.getComposedAsRectDimX()) / 2);
		double startPlayFieldYPosition = squareUnit
				+ ((canvas.getHeight() - squareUnit * ( pf.getComposedAsRectDimY()+4)) / 2);
		
		
		/*Definizione dei punti*/
		double[][] pointx = new double[pf.getComposedAsRectDimY()-1][pf.getComposedAsRectDimX()-1];
		double[][] pointy = new double[pf.getComposedAsRectDimY()-1][pf.getComposedAsRectDimX()-1];
		Color[][] pointColor = new Color[pf.getComposedAsRectDimY()-1][pf.getComposedAsRectDimX()-1];		
		double[][] pointxTran = new double[pf.getComposedAsRectDimX()-1][pf.getComposedAsRectDimY()-1];
		double[][] pointyTran = new double[pf.getComposedAsRectDimX()-1][pf.getComposedAsRectDimY()-1];
		Color[][] pointColorTran = new Color[pf.getComposedAsRectDimX()-1][pf.getComposedAsRectDimY()-1];
		
		pointx[0][0]=startPlayFieldXPosition;
		pointy[0][0]=startPlayFieldYPosition;
		pointxTran[0][0] = startPlayFieldXPosition;
		pointyTran[0][0] = startPlayFieldYPosition;

		for(int j=1;j<pointx.length;j++)
		{	
			pointx[j][0]=pointx[j-1][0];
			pointy[j][0]=pointy[j-1][0]+squareUnit+1;
			pointxTran[0][j] = pointxTran[0][j-1];
			pointyTran[0][j] = pointyTran[0][j-1]+squareUnit+1;
		}
		
		for(int i=1;i<pointx[0].length;i++)
		{
			pointx[0][i]=pointx[0][i-1]+squareUnit+1;
			pointy[0][i]=pointy[0][i-1];
			pointxTran[i][0] = pointxTran[i-1][0]+squareUnit+1;
			pointyTran[i][0] = pointyTran[i-1][0];
		}
		
		for(int j=2;j<pointx.length;j++)
		{
			for(int i=1;i<pointx[j].length;i++)
			{
				pointx[j][i]=pointx[j][i-1]+squareUnit+1;
				pointy[j][i]=pointy[j][i-1];
				pointxTran[i][j] = pointxTran[i-1][j]+squareUnit+1;
				pointyTran[i][j] = pointyTran[i-1][j];
			}
		}
		
		/*Proiezione dei colori*/
		for (int j = 2; j < pf.getComposedAsRectDimY()-1; j++) 
		{
			for (int i =1; i < pf.getComposedAsRectDimX()-1; i++) 
			{
				if (i >= pf.getMt().getxActual() + 1
						&& i <= pf.getMt().getxActual() + pf.getMt().getComposedAsRectDimX()
						&& j >= pf.getMt().getyActual() + 1
						&& j <= pf.getMt().getyActual() + pf.getMt().getComposedAsRectDimY()) 
				{
					if (pf.getMt().getPointPresence((byte) ((i - 1 - pf.getMt().getxActual()) % 4),
							(byte) ((j - 1 - pf.getMt().getyActual()) % 4))) {
						pointColor[j - 1][i - 1] = pf.getMt().gettType().getColor();
						pointColorTran[i - 1][j - 1] = pf.getMt().gettType().getColor();
						//System.out.println(pf);

					} else 
					{
						pointColor[j - 1][i - 1] = pf.getColori()[j][i];
						pointColorTran[i - 1][j - 1] = pf.getColori()[j][i];
					}
				}else 
				{
					pointColor[j - 1][i - 1] = pf.getColori()[j][i];
					pointColorTran[i - 1][j - 1] = pf.getColori()[j][i];
				}
			}
		}

		if(yInit<0)yInit=0;
		if(xInit<0)xInit=0;
		if(yFinal>pf.getComposedAsRectDimY()-3)yFinal=pf.getComposedAsRectDimY()-3;
		if(xFinal>pf.getComposedAsRectDimX()-3)xFinal=pf.getComposedAsRectDimX()-3;
		if(yInit<=yFinal||xInit<=xFinal)
		for(int j=yInit;j<=yFinal;j++)
			for(int i=xInit;i<=xFinal;i++) {
				canvas.getGraphicsContext2D().setStroke(pointColor[j][i]);;	
				canvas.getGraphicsContext2D().setFill(pointColor[j][i]);;
				canvas.getGraphicsContext2D().beginPath();
				canvas.getGraphicsContext2D().lineTo(pointx[j][i],pointy[j][i]);
				canvas.getGraphicsContext2D().lineTo(pointx[j][i+1],pointy[j][i+1]);
				canvas.getGraphicsContext2D().lineTo(pointx[j+1][i+1],pointy[j+1][i+1]);
				canvas.getGraphicsContext2D().lineTo(pointx[j+1][i],pointy[j+1][i]);
				canvas.getGraphicsContext2D().closePath();
				canvas.getGraphicsContext2D().fill();
				canvas.getGraphicsContext2D().stroke();
		}
	}

	public PlayField getPf() {
		return pf;
	}

}
