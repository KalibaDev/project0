package maestrelli.giacomo.ep;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import maestrelli.giacomo.playgrounds.drawable.JFXPlayField;

public class JFXMainLoop extends Application {

	private JFXPlayField pf;

	
	@Override
	public void start(Stage arg0)throws Exception {
		arg0.setTitle("T:AC JavaFX implementation");
		arg0.setHeight(480);
		arg0.setWidth(640);
		arg0.setX(450);
		arg0.setY(450);
		arg0.setMinWidth(320);
		arg0.setMinHeight(240);
		BorderPane bp = new BorderPane(pf);
		Scene sce = new Scene(bp,640,480,true,SceneAntialiasing.DISABLED);
		sce.setRoot(bp);
		
		MenuBar menuBar = new MenuBar();
		menuBar.setFocusTraversable(true);
		menuBar.setBackground(Background.EMPTY);
		
		menuBar.addEventHandler(MouseEvent.ANY,new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				pf.setPaused(true);
			}
		});
		
        Menu menuFile = new Menu("File");
        Menu menuEdit = new Menu("Option");
        Menu menuView = new Menu("About");
        
        menuBar.getMenus().addAll(menuFile,menuEdit,menuView);
        MenuItem newGame = new MenuItem("New game");
        MenuItem exitToDos = new MenuItem("Exit");
        MenuItem changeConfig = new MenuItem("Properties");
        MenuItem resume = new MenuItem("Resume");
        menuFile.getItems().addAll(newGame,exitToDos);
        menuEdit.getItems().addAll(resume,changeConfig);
        bp.setTop(menuBar);
		
		
		pf.requestFocus();
	    pf.setFocusTraversable(true);
		arg0.setScene(sce);
		arg0.show();
	}
	
	
	
    public JFXMainLoop() {
		super();
		try {
			pf = new JFXPlayField();
		} catch (MalformedURLException |URISyntaxException e) {
			e.printStackTrace();
			Platform.exit();
		}

	}

	public static void main(String[] args)
	{	
    	Application.launch(args);
    }

}
