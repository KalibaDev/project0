package maestrelli.giacomo.ep;

import java.util.HashMap;

import javafx.scene.input.KeyCode;

public class Configuration {
	
	public final static int MAXFORECASTPIECES = 1;
	public final static int STOPDELAY = 1000;
	public static float SPEED = 0.250f;
	public final static String ENDAUDIOTRACK ="end";
	public final static String MESSAGECANCELLED = "Cancelled!";
	public final static int KEYUP = 0;
	public final static int KEYDOWN = 1;
	public final static int KEYLEFT = 2;
	public final static int KEYRIGHT = 3;
	public final static int W = 4;
	public final static int SPACE = 5;
	public final static int PAUSE = 6;
	public final static int FIRSTUNUSED;
	
	public final static boolean gravity = true;
	public final static boolean bgm = false;
	
	
	//public final static KeyCode[] KEYCONFIGURATION = new KeyCode[] {KeyCode.UP,KeyCode.DOWN,KeyCode.LEFT,KeyCode.RIGHT};
	public final static HashMap<KeyCode,Integer> hmKI = new HashMap<KeyCode,Integer>();
	static
	{
		hmKI.put(KeyCode.W,KEYUP);
		hmKI.put(KeyCode.DOWN,KEYDOWN);
		hmKI.put(KeyCode.Q,KEYLEFT );
		hmKI.put(KeyCode.E,KEYRIGHT);
		hmKI.put(KeyCode.L,W);
		hmKI.put(KeyCode.P,PAUSE);
		hmKI.put(KeyCode.SPACE,SPACE);
		FIRSTUNUSED = hmKI.size()+1;
	}
	
	
}
